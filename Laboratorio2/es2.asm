            .data
prompt:     .asciiz "Inserisci un numero intero positivo:"
error:      .asciiz "Valore non rappresentabile su byte!\n"
error2:     .asciiz "Valore negativo!\n"
result:     .asciiz "Il risultato dell'operazione è:"
maxByte:    .byte   255

            .text
            .globl main
main:   
            lbu		$t3, maxByte($0)	# Load max byte value 
            
read1:      li      $v0,    4           # Print string
            la		$a0,    prompt	    # Load string 
            syscall 

            li      $v0,    5           # Read integer
            syscall 
            move 	$t0,    $v0		    # $t0 =  $v0

checkPos1:  bge		$t0, $0, checkByte1 # if $t0 >= $0 then checkByte1
            li      $v0,    4           # Print string
            la		$a0,    error2	    # Load string 
            syscall
            j		read1				# jump to read1
checkByte1: ble		$t0, $t3, read2	    # if $t0 <= $t3 then read2
            li      $v0,    4           # Print string
            la		$a0,    error	    # Load string 
            syscall
            j		read1				# jump to read1
            

read2:      li      $v0,    4           # Print string
            la		$a0,    prompt	    # Load string 
            syscall 

            li      $v0,    5           # Read integer
            syscall 
            move 	$t1,    $v0		    # $t1 =  $v0

checkPos2:  bge		$t1, $0, checkByte2 # if $t1 >= $0 then checkByte2
            li      $v0,    4           # Print string
            la		$a0,    error2	    # Load string 
            syscall
            j		read1				# jump to read1
checkByte2: ble		$t1, $t3, continue  # if $t1 <= $t3 then continue
            li      $v0,    4           # Print string
            la		$a0,    error	    # Load string 
            syscall
            j		read2				# jump to read2

continue:   nor     $t4, $t1, $t1       # $t4 = NOT(B)
            and     $t4, $t0, $t4       # $t4 = A AND (NOT(B))
            nor     $t4, $t4, $t4       # $t4 = NOT(A AND (NOT(B)))
            xor     $t5, $t0, $t1       # $t5 = A XOR B
            or      $t4, $t4, $t5       # $t4 = NOT(A AND (NOT(B))) OR (A XOR B)

            li      $v0,    4           # Print string
            la		$a0,    result	    # Load string 
            syscall 

            li      $v0,    1           # Print integer
            move 	$a0, $t4		    # $a0 = $t4
            syscall

            li      $v0,    10          # Exit
            syscall