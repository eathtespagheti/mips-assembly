        .data
var:    .half   0x0FA6
prompt1:.asciiz "There are "
prompt2:.asciiz " ones in the birany rappresentation of "
newline:.byte   '\n'

        .text
        .globl main
main:   
        move 	$t1, $0		        # $t1 = $0
        lh      $t0, var($0)

loop:   beq	$t0, $0, result	    # if $t0 == $0 then result
        andi    $t2, $t0, 1         # And between the read value and 1
        beq     $t2, $0, even       # If it's an even number there's a 0 as the last digit
        addi	$t1, $t1, 1         # $t1 = $t1 + 1
even:   srl     $t0, $t0, 1
        j	loop		    # jump to loop
        
        

result: li      $v0,    4           # Print string
        la		$a0,    prompt1	    # Load string 
        syscall 
        li      $v0,    1           # Print integer
        move 	$a0,    $t1		    # $a0 = $t1
        syscall
        li      $v0,    4           # Print string
        la		$a0,    prompt2	    # Load string 
        syscall 
        li      $v0,    1           # Print integer
        lh 	$a0,    var($0)     # $a0 = $t0
        syscall
        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall
