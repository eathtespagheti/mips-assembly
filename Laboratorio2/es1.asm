            .data
prompt:     .asciiz "Inserisci un numero intero:"
outpari:    .asciiz "Il numero inserito è pari"
outdispari: .asciiz "Il numero inserito è dispari"

            .text
            .globl main
main:   
            li      $v0,    4           # Print string
            la		$a0,    prompt	    # Load string 
            syscall 

            li      $v0,    5           # Read integer
            syscall 
            move 	$t0,    $v0		    # $t0 =  $v0

            andi    $t1, $t0, 1         # And between the read value and 1

            li      $v0,    4           # Print string
            bne		$t1, $0, dispari	# if $t1 != $0 then dispari           
pari:       la		$a0,    outpari	    # Load string
            j		print		        # jump to print
dispari:    la		$a0,    outdispari	# Load string
print:      syscall 

            li      $v0,    10          # Exit
            syscall