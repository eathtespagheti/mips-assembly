        .data
var1:   .word   5
var2:   .word   13
var3:   .word   -8
val1:   .asciiz "First value it's equal to: "
val2:   .asciiz "Second value it's equal to: "
val3:   .asciiz "Third value it's equal to: "
newline:.byte   '\n'

        .text
        .globl main
main:   
        lw		$t1, var1($0)		# Load var1 in $t1
        lw		$t2, var2($0)		# Load var2 in $t2
        lw		$t3, var3($0)		# Load var3 in $t3

abcheck:ble		$t1, $t2, accheck	# if $t1 <= $t2 then accheck
        # Swap values
        move 	$t4, $t1		    # $t4 = $t1
        move 	$t1, $t2		    # $t1 = $t2
        move 	$t2, $t4		    # $t2 = $t4
accheck:ble		$t1, $t3, bccheck    # if $t1 <= $t3 then bccheck
        # Swap values   
        move 	$t4, $t1		    # $t4 = $t1
        move 	$t1, $t3		    # $t1 = $t3
        move 	$t3, $t4		    # $t3 = $t4
bccheck:ble		$t2, $t3, print     # if $t2 <= $t3 then print
        # Swap values   
        move 	$t4, $t2		    # $t4 = $t2
        move 	$t2, $t3		    # $t2 = $t3
        move 	$t3, $t4		    # $t3 = $t4

print:  lb		$t4, newline($0)	# Load newline char 

        li      $v0,    4           # Print string
        la		$a0,    val1	    # Load string 
        syscall 
        li      $v0,    1           # Print integer
        move 	$a0,    $t1		    # $a0 = $t1
        syscall
        li      $v0,    11          # Print char
        move    $a0,    $t4		    # $a0 = $t4
        syscall

        li      $v0,    4           # Print string
        la		$a0,    val2	    # Load string 
        syscall 
        li      $v0,    1           # Print integer
        move 	$a0,    $t2		    # $a0 = $t2
        syscall
        li      $v0,    11          # Print char
        move    $a0,    $t4		    # $a0 = $t4
        syscall

        li      $v0,    4           # Print string
        la		$a0,    val3	    # Load string 
        syscall 
        li      $v0,    1           # Print integer
        move 	$a0,    $t3		    # $a0 = $t3
        syscall
        li      $v0,    11          # Print char
        move    $a0,    $t4		    # $a0 = $t4
        syscall
        