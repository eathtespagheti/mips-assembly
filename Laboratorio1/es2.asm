        .data   
var1:   .byte   'm'
var2:   .byte   'i'
var3:   .byte   'p'
var4:   .byte   's'
var5:   .byte   0
offset: .byte   32

        .text
        .globl main
main:
        lb      $t3, offset($0) # Load offest value

        lb		$t1, var1($0)   # Load the character in $t0 
        sub	    $t0, $t1, $t3   # Save the uppercase character
        sb		$t0, var1($0)   # Save the updated character in memory

        lb		$t1, var2($0)   # Load the character in $t0 
        sub 	$t0, $t1, $t3   # Save the uppercase character
        sb		$t0, var2($0)   # Save the updated character in memory

        lb		$t1, var3($0)   # Load the character in $t0 
        sub	    $t0, $t1, $t3   # Save the uppercase character
        sb		$t0, var3($0)   # Save the updated character in memory

        lb		$t1, var4($0)   # Load the character in $t0 
        sub 	$t0, $t1, $t3   # Save the uppercase character
        sb		$t0, var4($0)   # Save the updated character in memory
        
        la		$a0 , var1($0)  # Load string address in parameter
        li		$v0,    4       # Load print string syscall
        syscall                 # Print string

        li      $v0, 10         # Exit
        syscall