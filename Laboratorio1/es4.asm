        .data
var:    .word   0x3FFFFFF0
n:      .asciiz "\n"
        
        .text
        .globl  main
main:
        lw      $t1, var($0)    # Load word in $t1
        addu    $t1, $t1, $t1   # $t1 = $t1 + $t1

        move    $a0, $t1        # Copy value in a0
        li		$v0, 1          # print integer syscall
        syscall

        la		$a0, n($0)		# Add newline in arg
        li		$v0, 4          # Print string syscall
        syscall

        addiu   $t3, $t1, 40    # $t3 = $t1 + 40
        move    $a0, $t3        # Copy value in a0
        li		$v0, 1          # print integer syscall
        syscall

        la		$a0, n($0)		# Add newline in arg
        li		$v0, 4          # Print string syscall
        syscall

        li		$t2, 40		    # $t2 = 40
        addu    $t3, $t1, $t2   # $t3 = $t1 + $t2
        move    $a0, $t3        # Copy value in a0
        li		$v0, 1          # print integer syscall
        syscall

        li      $v0, 10         # Exit
        syscall
