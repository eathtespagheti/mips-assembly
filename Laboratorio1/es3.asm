        .data
op1:    .byte   150
op2:    .byte   100

        .text
        .globl main
main:
        lbu		$t2, op1($0)
        lbu		$t1, op2($0)
        add		$t0, $t1, $t2   # $t0 = $t1 + $t2
        
        li		$v0, 1		    # $v0 = 1
        move 	$a0, $t0        # $a0 = $t0
        syscall

        li      $v0, 10         # Exit
        syscall
