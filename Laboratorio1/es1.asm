        .data
n1:     .byte   10
n2:     .byte   0x10
n3:     .byte   '1'
res:    .space  1

        .text
        .globl  main
main:
        # Load variable in registers
        lb		$t1, n1		# 
        lb		$t2, n2		# 
        lb		$t3, n3		# 
        
        # Do things
        sub		$t0, $t1, $t2		# $t0 = $t1 - $t2
        add		$t0, $t0, $t3	# $t1 = $t0 + n3

        # Save in res
        sb		$t0, res($0)

        # Exit
        li		$v0, 10		# $v0 = 10
        syscall
        
        
        
        