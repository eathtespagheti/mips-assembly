        .data
prompt: .asciiz "Insert a number:"
return: .asciiz "\n"
space:  .asciiz " - "

        .text
        .globl  main
main:
        li  $v0, 4          # $v0 = 4 print string
        la  $a0, prompt($0) # Load string
        syscall
        li  $v0, 5          # $v0 = 5 read int
        syscall
        move $t1, $v0

        li  $v0, 4          # $v0 = 4 print string
        la  $a0, prompt($0) # Load string
        syscall
        li  $v0, 5          # $v0 = 5 read int
        syscall
        move $t2, $v0

        li   $v0, 1         # Print int
        move $a0, $t1		# $a0 = $t1
        syscall
        li  $v0, 4          # $v0 = 4 print string
        la  $a0, space($0)  # Load space
        syscall
        li   $v0, 1         # Print int
        move $a0, $t2		# $a0 = $t2
        syscall
        li  $v0, 4          # $v0 = 4 print string
        la  $a0, return($0) # Load return
        syscall

        # Swap the values
        add	 $t2, $t1, $t2		# $t2 = $t1 + $t2
        sub	 $t1, $t2, $t1		# $t1 = $t2 - $t1
        sub	 $t2, $t2, $t1		# $t2 = $t2 - $t1

        li   $v0, 1         # Print int
        move $a0, $t1		# $a0 = $t1
        syscall
        li  $v0, 4          # $v0 = 4 print string
        la  $a0, space($0)  # Load space
        syscall
        li   $v0, 1         # Print int
        move $a0, $t2		# $a0 = $t2
        syscall
        li  $v0, 4          # $v0 = 4 print string
        la  $a0, return($0) # Load return
        syscall
        
        li      $v0, 10         # Exit
        syscall
        