        .data
dim:    .byte   4
wSize:  .byte   4
out:    .asciiz "La media è: "
in:     .asciiz "Inserisci un valore: "
newline:.byte   '\n'

        .text
        .globl main
main:
        move 	$t0, $0		# $t0 = $0
        lb		$s0, dim($0)		# 
        lb		$s1, wSize($0)		# 

loop:   beq		$t0, $s0, print	# if $t0 == $s0 then print
        li  $v0, 4
        la  $a0, in
        syscall
        li		$v0, 5		# $v0 = 5
        syscall
        addu	$t1, $t1, $v0
        addiu   $t0, $t0, 1
        j		loop				# jump to loop
        

print:  divu	$t1, $t0			# $t1 / $t0
        mflo	$t1					# $t1 = floor($t1 / $t0) 

        li  $v0, 4
        la  $a0, out
        syscall

        li    $v0, 1
        move 	$a0, $t1		# $a0   = $t1
        syscall

        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall

end:    li  $v0, 10
        syscall        
        