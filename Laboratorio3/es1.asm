            .data
prompt:     .asciiz "Inserisci un numero:"
out:        .asciiz "Il numero inserito è: "
newline:    .byte   '\n'
offset:     .byte   '0'
base:       .byte   10

            .text
            .globl main
main:
        li      $v0,    4       # Print string
        la	$a0,    prompt  # Load string 
        syscall 

        lb      $s0, newline($0)# Load newline char
        lb      $s1, base($0)   # Load base value
        lb      $s2, offset($0) # Load offset char
        move 	$t0, $0         # $t1 = $0 Initialize number

loop:   li      $v0,    12      # Read char
        syscall
        beq     $v0, $s0, print # if character it's \n
        sub     $v0, $v0, $s2   # $v0 = $v0 - $s1 remove offset from read
        multu	$t0, $s1        # Multiply by base the number
        mflo	$t0             # copy Lo to $t0
        addu    $t0, $t0, $v0   # $t0 = $t0 + $v0
        j       loop            # jump to loop

print:  li      $v0,    4       # Print string
        la      $a0,    out     # Load string 
        syscall 

        li      $v0,    1       # Print integer
        move    $a0,    $t0     # Load int 
        syscall 

        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall

end:    li      $v0,    10          # Exit
        syscall