            .data
prompt:     .asciiz "Inserisci un numero:"
out:        .asciiz "Il numero inserito è: "
nodigitp:   .asciiz "\nIl carattere inserito non è un numero!\n"
overfp:     .asciiz "\nIl numero non è rappresentabile in 4 byte!\n"
newline:    .byte   '\n'
offset:     .byte   '0'
max:        .byte   '9'
base:       .byte   10

            .text
            .globl main
main:
        lb      $s0, newline($0)# Load newline char
        lb      $s1, base($0)   # Load base value
        lb      $s2, offset($0) # Load offset char
        lb      $s3, max($0)    # Load max char
        move 	$t0, $0         # $t1 = $0 Initialize number

printp: li      $v0,    4       # Print string
        la      $a0,    prompt  # Load string 
        syscall

loop:   li      $v0,    12      # Read char
        syscall
        beq     $v0, $s0, print     # if character it's \n
        bgt     $v0, $s3, nodigit   # if char major than '9' then nodigit
        blt     $v0, $s2, nodigit   # if char minor than '0' then nodigit
        subu    $v0, $v0, $s2       # $v0 = $v0 - $s1 remove offset from read
        multu	$t0, $s1            # Multiply by base the number
        mfhi    $t1                 # Move hi register to $t1
        bne     $t1, $0, overf      # if $t1 != $0 then overf
        mflo	$t0                 # copy Lo to $t0
        addu    $v0, $t0, $v0       # $v0 = $t0 + $v0
        bltu    $v0, $t1,  overf    # if result it's smaller than the operand then there is overflow
        move    $t0, $v0
        j       loop                # jump to loop

nodigit:li      $v0,    4           # Print string
        la      $a0,    nodigitp    # Load string 
        syscall
        j       printp              # jump to printp

overf:  li      $v0,    4       # Print string
        la      $a0,    overfp  # Load string 
        syscall
        j       end             # jump to loop
        

print:  li      $v0,    4       # Print string
        la      $a0,    out     # Load string 
        syscall 

        li      $v0,    1       # Print integer
        move    $a0,    $t0     # Load int 
        syscall 

        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall

end:    li      $v0,    10          # Exit
        syscall
