            .data
giorni:     .byte   100
ore:        .byte   10
minuti:     .byte   30
overflowP:  .asciiz "\nOverflow detected!\nAborting...\n"
out:        .asciiz "Number of minutes: "
newline:    .byte   '\n'
risultato:  .word   0

            .text
            .globl main
main:
        lb  $s0, giorni($0) # Load number of days
        lb  $s1, ore($0)    # Load number of hours
        lb  $s2, minuti($0) # Load number of minutes

        li      $t1, 24     # $t1 = 24
        multu   $s0, $t1
        # Check multiplication overflow
        mfhi    $t2
        bne     $t2, $0, abort  # if $t2 != $0 then abort
        mflo    $t0
        addu    $t0, $s1, $t0   # $t0 = $s1 + $t0
        # Check add overflow
        bltu    $t0, $s1, abort  # if $t0 < $s1 then abort
        move    $s1, $t0

        li      $t1, 60     # $t1 = 60
        multu   $s1, $t1
        # Check multiplication overflow
        mfhi    $t2
        bne     $t2, $0, abort  # if $t2 != $0 then abort
        mflo    $t0
        addu    $t0, $s2, $t0   # $t0 = $s2 + $t0
        # Check add overflow
        bltu    $t0, $s2, abort  # if $t0 < $s2 then abort
        move    $s2, $t0
        
        sw      $s2, risultato($0)
        
        
print:  li  $v0, 4
        la  $a0, out
        syscall

        li    $v0, 1
        lw    $a0, risultato($0)
        syscall

        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall

        j   end     # jump to end
          

abort:  li  $v0, 4
        la  $a0, overflowP
        syscall

end:    li  $v0, 10
        syscall
                

        