        .data
opa:    .word   2043
opb:    .word   5
prompt: .asciiz "0 -> res = a + b\n1 -> res = a - b\n2 -> res = a * b\n3 -> res = a / b\nScegli l'operazione: "
rprint: .asciiz "Il risultato è: "
errorp: .asciiz "\nInput non valido\n"
newline:.byte   '\n'

        .text
        .globl main
main:
        li  $v0,    4
        la  $a0,    prompt
        syscall

        li  $v0,    5
        syscall

        lw  $t1,    opa($0)
        lw  $t2,    opb($0)
        
case0:  move    $t3,    $0              # $t3 = $0
        bne     $v0,    $t3,    case1   # if $v0 != $t3 then case1
        add     $t0,    $t1,    $t2 
        j       print                   # jump to print
case1:  addi    $t3,    $t3,    1       # $t3 = $t3 + 1
        bne     $v0,    $t3,    case2   # if $v0 != $t3 then case2
        sub     $t0,    $t1,    $t2 
        j       print                   # jump to print
case2:  addi    $t3,    $t3,    1       # $t3 = $t3 + 1
        bne     $v0,    $t3,    case3   # if $v0 != $t3 then case3
        mult    $t1,    $t2             # $t1 * $t2 = Hi and Lo registers
        mflo    $t0                     # copy Lo to $t0
        j       print                   # jump to print
case3:  addi    $t3,    $t3,    1       # $t3 = $t3 + 1
        bne     $v0,    $t3,    error   # if $v0 != $t3 then error
        div     $t1,    $t2             # $t1 / $t2
        mflo    $t0                     # $t0 = floor($t1 / $t2)
        j       print                   # jump to print
        
print:  li      $v0,    4
        la      $a0,    rprint
        syscall

        li      $v0,    1
        move    $a0,    $t0        # $a0 = $t0
        syscall

        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall

        j       end                 # jump to end

error:  li  $v0,    4
        la  $a0,    errorp
        syscall
        j   main            # jump to main
        

end:    li  $v0,    10
        syscall  