                .data
.eqv            DIM_CELLA       4
.eqv            NUM_CELLE       10
.eqv            SIDE_SIZE       40      # DIM_CELLA * NUM_CELLE
.eqv            MATRIX_SIZE     400     # DIM_CELLA * NUM_CELLE * NUM_CELLE
.eqv            X_Index         $t0
matrix:         .space  MATRIX_SIZE
newline:        .byte   '\n'
tab:            .byte   '\t'

                .text
                .globl main
main:
        li      X_Index, 1                  # X_Index = 1  --> Index X
        li      $t1, 1                  # $t1 = 1  --> Index Y
        move    $t4, $0                 # $t4 = $0 --> Matrix Index
        li      $s0, DIM_CELLA          # $s0 = Jump size
        li      $s1, NUM_CELLE          # $s1 = Numero di celle per riga/colonna

loopY:  bgt     $t1, $s1, end   # if $t1 > $s1 then end
        li      X_Index, 1          # X_Index = 1 --> Index X
loopX:  bgt     X_Index, $s1, endLY # if X_Index > $s1 then endLY
        mult    X_Index, $t1
        mflo    $a0             # copy Lo to $a0        
        li      $v0, 1
        syscall
        sw      $a0, matrix($t4)# Save value in matrix 
        add     $t4, $t4, $s0   # $t4 = $t4 + $s0
        li      $v0, 11         # Print char
        lb      $a0, tab($0)    # $a0 = tab
        syscall
        addi    X_Index, X_Index, 1     # X_Index = X_Index + 1
        j       loopX           # jump to loopX
endLY:  li      $v0, 11         # Print char
        lb      $a0, newline($0)# $a0 = newline
        syscall
        addi    $t1, $t1, 1     # $t1 = $t1 + 1
        j       loopY           # jump to loopY
        

end:    li  $v0,    10
        syscall  