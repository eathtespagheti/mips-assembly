        .data
array:  .space  80
newline:.byte   '\n'

        .text
        .globl  main
main:
        move 	$t0,    $0
        li      $t1,    80
        li      $t3,    8
        li      $t4,    1

preloop:    beq     $t0,    $t3,    ploop    # if $t0 == $t3 then loop
            sw      $t4,    array($t0) # Save 1 in the array
            # Print value
            li      $v0,    1
            move     $a0, $t4        # $a0   = $t4
            syscall
            li      $v0,    11          # Print char
            lb      $a0,    newline($0) # $a0 = newline
            syscall
            # Increase counter
            addi    $t0,    $t0,    4
            j       preloop                # jump to preloop



        # t0    =   loop index
        # t1    =   loop max
        # t3    =   t0 - 2
        # t4    =   array[t0-1]
        # t6    =   value for array[t3]
ploop:  move 	$t3,    $0
loop:   bge     $t0,    $t1,    end
        lw		$t6,    array($t3)
        add     $t4,    $t4,    $t6
        sw		$t4,    array($t0)
        # Print value
        li      $v0,    1
        move     $a0, $t4        # $a0   = $t4
        syscall
        li      $v0,    11          # Print char
        lb      $a0,    newline($0) # $a0 = newline
        syscall
        # Increase counter
        addi    $t0,    $t0,    4
        addi    $t3,    $t3,    4
        j        loop               # jump to loop
        

end:    li      $v0,    10
        syscall        
        