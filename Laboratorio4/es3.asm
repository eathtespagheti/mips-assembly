                .data
.eqv            DIM_CELLA       4
.eqv            NUM_CELLE       4
.eqv            ARRAY_SIZE      16      # DIM_CELLA * NUM_CELLE
.eqv            MATRIX_SIZE     64      # DIM_CELLA * ARRAY_SIZE
arrayX:         .word   5, 15, 25, 8
arrayY:         .word   1, 2, 3, 4
matrix:         .space  MATRIX_SIZE
newline:        .byte   '\n'
tab:            .byte   '\t'

                .text
                .globl main
main:
        move    $t0, $0                 # $t0 = $0 --> Index X
        move    $t1, $0                 # $t1 = $0 --> Index Y
        move    $t4, $0                 # $t4 = $0 --> Matrix Index
        li      $s0, DIM_CELLA          # $s0 = Jump size
        li      $s1, ARRAY_SIZE         # $s1 = Numero byte occupati da un singolo array

loopY:  bge     $t1, $s1, end   # if $t1 > $s1 then end
        move    $t0, $0         # $t0 = $0 --> Index X
loopX:  bge     $t0, $s1, endLY # if $t0 > $s1 then endLY
        lw      $t2, arrayX($t0)
        lw      $t3, arrayY($t1)
        mult    $t2, $t3        # $t2 * $t3 = Hi and Lo registers
        mflo    $a0             # copy Lo to $a0        
        li      $v0, 1
        syscall
        sw      $a0, matrix($t4)# Save value in matrix 
        add     $t4, $t4, $s0   # $t4 = $t4 + $s0
        li      $v0, 11         # Print char
        lb      $a0, tab($0)    # $a0 = tab
        syscall
        add     $t0, $t0, $s0   # $t0 = $t0 + s0
        j       loopX           # jump to loopX
endLY:  li      $v0, 11         # Print char
        lb      $a0, newline($0)# $a0 = newline
        syscall
        add     $t1, $t1, $s0   # $t1 = $t1 + s0
        j       loopY           # jump to loopY
        

end:    li  $v0,    10
        syscall  