            .data
matrix:     .word       154, 123, 0109, 86, 004, 0,
                        412, -23, -231, 09, 050, 0,
                        123, -24, 0012, 55, -45, 0,
                        000, 000, 0000, 00, 000, 0
.eqv        X_SIZE      6
.eqv        Y_SIZE      4
.eqv        INCREMENT_VALUE 4
.eqv        X_Index     $t0
.eqv        Y_Index     $t1
.eqv        MatrixIndex $t3
.eqv        RowSum      $t4
.eqv        Increment   $s0
.eqv        XLimit      $s1
.eqv        YLimit      $s2
.eqv        RowLenght   $s3
newline:    .byte       '\n'
tab:        .byte       '\t'
        
        .text
        .globl main
main:
        li      XLimit, X_SIZE              
        li      YLimit, Y_SIZE 
        li      Y_Index, 1             
        li      Increment, INCREMENT_VALUE
        move    MatrixIndex, $0         # MatrixIndex = $0  --> Matrix Index
        mult    XLimit, Increment       # XLimit * Increment = Hi and Lo registers
        mflo    RowLenght               # copy Lo to RowLenght
        

loopY:  
        bge     Y_Index, YLimit, preloopA  # if Y_Index >   then preloopA
        move    RowSum, $0              # RowSum = $0
        li      X_Index, 1
loopX:  
        bge     X_Index, XLimit, endLY  # if X_Index > XLimit then endLY
        lw      $t5, matrix(MatrixIndex) 
        add     RowSum, RowSum, $t5     # RowSum = RowSum + $t5
        add     MatrixIndex, MatrixIndex, Increment
        addi    X_Index, X_Index, 1     # X_Index = X_Index + 1
        j       loopX                   # jump to loopX
endLY:  
        li      $v0, 1                  # $v0 = 1
        move    $a0, $t4                # $a0 = $t4
        syscall
        sw      $a0, matrix(MatrixIndex)# Save value in matrix 
        add     MatrixIndex, MatrixIndex, Increment
        addi    Y_Index, Y_Index, 1     # Y_Index = Y_Index + 1
        li      $v0, 11                 # Print char
        lb      $a0, newline($0)        # $a0 = newline
        syscall
        j       loopY                   # jump to loopY

preloopA:
        syscall
        move    X_Index, $0        # PRELOOP A X_Index = $0
        subi    YLimit, YLimit, 1            # YLimit = YLimit - 1
        subi    XLimit, XLimit, 1            # XLimit = XLimit - 1
loopA:  
        bgt     X_Index, XLimit, end    
        move    RowSum, $0              # RowSum = $0
        move    Y_Index, $0              # Y_Index = $0
loopB:  
        bge     Y_Index, YLimit, endLB
        multu   Y_Index, RowLenght            # Y_Index * RowLenght = Hi and Lo registers
        mflo    $t5                    # copy Lo to $t5
        multu   X_Index, Increment            # X_Index * $t5 = Hi and Lo registers
        mflo    MatrixIndex                    # copy Lo to MatrixIndex
        addu    MatrixIndex, MatrixIndex, $t5

        lw      $t5, matrix(MatrixIndex) 
        add     RowSum, RowSum, $t5     # RowSum = RowSum + $t5

        addi    Y_Index, Y_Index, 1     # Y_Index = Y_Index + 1
        j       loopB                  # jump to loopB
endLB:  
        addiu   X_Index, X_Index, 1
        li      $v0, 1                  # $v0 = 1
        move    $a0, $t4                # $a0 = $t4
        syscall
        addu    MatrixIndex, MatrixIndex, RowLenght
        sw      $a0, matrix(MatrixIndex)# Save value in matrix 
        li      $v0, 11                 # Print char
        lb      $a0, newline($0)        # $a0 = newline
        syscall
        j       loopA                  # jump to loopA  

end:    li  $v0,    10
        syscall