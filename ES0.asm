        .data
prompt: .asciiz "\n Please Input a value for N = " 
result: .asciiz " The sum of the integers from 1 to N is " 
bye:    .asciiz "\n **** Adios Amigo - Have a good day ****" 
        .globl  main 
        .text 
main: 
        li      $v0, 4          # system call code for Print String 
        la      $a0, prompt     # load address of prompt into $a0 
        syscall                 # print the prompt message 

        li      $v0, 5          # system call code for Read Integer 
        syscall                 # reads the value of N into $v0 

        blez    $v0, end        # branch to end if $v0 < = 0 
        li      $t0, 0          # clear register $t0 to zero 
loop:
        add     $t0, $t0, $v0   # sum of integers in register $t0
        addi    $v0, $v0, -1    # $v0--
        bnez    $v0, loop       # Go back to loop if $v0 != 0

        li      $v0, 4          # System call for Print String
        la      $a0, result     # Load addess of result in $a0
        syscall                 # print the string

        li      $v0, 1          # System call for print integer
        move 	$a0, $t0		# $a0 = $t0
        syscall
        b		main			# branch to main
end:
        li      $v0, 4          # load instruction for printing
        la      $a0, end        # load addres for end string
        syscall                 # call system instruction

        li      $v0, 10         # instruction for ending program and give back control to system
        syscall


